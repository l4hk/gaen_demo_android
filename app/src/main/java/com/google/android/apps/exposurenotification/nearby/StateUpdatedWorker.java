/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.nearby;

import android.content.Context;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.hilt.Assisted;
import androidx.hilt.work.WorkerInject;
import androidx.work.ListenableWorker;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkerParameters;
import com.google.android.apps.exposurenotification.R;
import com.google.android.apps.exposurenotification.common.NotificationHelper;
import com.google.android.apps.exposurenotification.common.Qualifiers.BackgroundExecutor;
import com.google.android.apps.exposurenotification.common.Qualifiers.ScheduledExecutor;
import com.google.android.apps.exposurenotification.common.TaskToFutureAdapter;
import com.google.android.apps.exposurenotification.common.time.Clock;
import com.google.android.apps.exposurenotification.keydownload.DiagnosisKeyDownloader;
import com.google.android.apps.exposurenotification.logging.AnalyticsLogger;
import com.google.android.apps.exposurenotification.proto.ApiCall;
import com.google.android.apps.exposurenotification.proto.WorkManagerTask.WorkerTask;
import com.google.android.apps.exposurenotification.riskcalculation.ClassificationThreshold;
import com.google.android.apps.exposurenotification.riskcalculation.DailySummaryRiskCalculator;
import com.google.android.apps.exposurenotification.riskcalculation.ExposureClassification;
import com.google.android.apps.exposurenotification.riskcalculation.RevocationDetector;
import com.google.android.apps.exposurenotification.storage.ExposureEntity;
import com.google.android.apps.exposurenotification.storage.ExposureNotificationSharedPreferences;
import com.google.android.apps.exposurenotification.storage.ExposureNotificationSharedPreferences.BadgeStatus;
import com.google.android.apps.exposurenotification.storage.ExposureRepository;
import com.google.android.gms.nearby.exposurenotification.CalibrationConfidence;
import com.google.android.gms.nearby.exposurenotification.DailySummariesConfig;
import com.google.android.gms.nearby.exposurenotification.ExposureWindow;
import com.google.android.gms.nearby.exposurenotification.Infectiousness;
import com.google.android.gms.nearby.exposurenotification.ReportType;
import com.google.android.gms.nearby.exposurenotification.ScanInstance;
import com.google.android.gms.tasks.Task;
import com.google.common.util.concurrent.FluentFuture;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import org.threeten.bp.Duration;
import org.threeten.bp.Instant;

/**
 * Performs work for {@value com.google.android.gms.nearby.exposurenotification.ExposureNotificationClient#ACTION_EXPOSURE_STATE_UPDATED}
 * broadcast from exposure notification API.
 */
public class StateUpdatedWorker extends ListenableWorker {

  private static final String TAG = "StateUpdatedWorker";

  private static final Duration GET_DAILY_SUMMARIES_TIMEOUT = Duration.ofSeconds(120);

  private final Context context;
  private final ExposureRepository exposureRepository;
  private final ExposureNotificationClientWrapper exposureNotificationClientWrapper;
  private final RevocationDetector revocationDetector;
  private final DailySummariesConfig dailySummariesConfig;
  private final DailySummaryRiskCalculator dailySummaryRiskCalculator;
  private final NotificationHelper notificationHelper;
  private final ExecutorService backgroundExecutor;
  private final ScheduledExecutorService scheduledExecutor;
  private final AnalyticsLogger logger;
  private final Clock clock;
  private final DiagnosisKeyDownloader downloader;

  ExposureNotificationSharedPreferences exposureNotificationSharedPreferences;

  @WorkerInject
  public StateUpdatedWorker(
      @Assisted @NonNull Context context,
      @Assisted @NonNull WorkerParameters workerParams,
      ExposureRepository exposureRepository,
      ExposureNotificationClientWrapper exposureNotificationClientWrapper,
      ExposureNotificationSharedPreferences exposureNotificationSharedPreferences,
      RevocationDetector revocationDetector,
      DailySummariesConfig dailySummariesConfig,
      DailySummaryRiskCalculator dailySummaryRiskCalculator,
      NotificationHelper notificationHelper,
      @BackgroundExecutor ExecutorService backgroundExecutor,
      @ScheduledExecutor ScheduledExecutorService scheduledExecutor,
      AnalyticsLogger logger,
      Clock clock,
      DiagnosisKeyDownloader downloadController) {
    super(context, workerParams);
    this.context = context;
    this.exposureRepository = exposureRepository;
    this.exposureNotificationClientWrapper = exposureNotificationClientWrapper;
    this.exposureNotificationSharedPreferences = exposureNotificationSharedPreferences;
    this.revocationDetector = revocationDetector;
    this.dailySummariesConfig = dailySummariesConfig;
    this.dailySummaryRiskCalculator = dailySummaryRiskCalculator;
    this.notificationHelper = notificationHelper;
    this.backgroundExecutor = backgroundExecutor;
    this.scheduledExecutor = scheduledExecutor;
    this.logger = logger;
    this.clock = clock;
    this.downloader = downloadController;
  }

  static void runOnce(WorkManager workManager) {
    workManager.enqueue(new OneTimeWorkRequest.Builder(StateUpdatedWorker.class).build());
  }

  @NonNull
  @Override
  public ListenableFuture<Result> startWork() {

    return FluentFuture.from(downloader.downloadDailySummaryConfig())
            .transformAsync((dailySummariesConfig) -> TaskToFutureAdapter.getFutureWithTimeout(
                exposureNotificationClientWrapper.getDailySummaries(dailySummariesConfig),
                GET_DAILY_SUMMARIES_TIMEOUT,
                scheduledExecutor), backgroundExecutor)
            .transform(
                (map) -> {
                  logger.logWorkManagerTaskStarted(WorkerTask.TASK_STATE_UPDATED);
                  List<DailySummaryWrapper> dailySummaries = (List<DailySummaryWrapper>) map.get("dailySummaryWrappers");
                  retrievePreviousExposuresAndCheckForExposureUpdate(context, dailySummaries, (ClassificationThreshold) map.get("thresholdConfig"));
                  logger.logWorkManagerTaskSuccess(WorkerTask.TASK_STATE_UPDATED);
                  return Result.success();
                },
                backgroundExecutor)
            .catching(
                Exception.class,
                x -> {
                  Log.e(TAG, "Failure to update app state (tokens, etc) from exposure summary.", x);
                  logger.logWorkManagerTaskFailure(WorkerTask.TASK_STATE_UPDATED, x);
                  return Result.failure();
                }, backgroundExecutor);
  }

  private void retrievePreviousExposuresAndCheckForExposureUpdate(Context context,
    List<DailySummaryWrapper> dailySummaries, ClassificationThreshold threshold) {
    List<ExposureEntity> currentExposureEntities =
        revocationDetector.dailySummaryToExposureEntity(dailySummaries);

    ExposureClassification currentClassification =
            dailySummaryRiskCalculator.classifyExposure(dailySummaries, threshold);

    ExposureClassification previousClassification =
            exposureNotificationSharedPreferences.getExposureClassification();

    checkForExposureUpdate(context, currentExposureEntities, currentClassification,
            previousClassification);

    ListeningExecutorService executor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(10));

    Callable<Task<List<ExposureWindow>>> asyncTask = () -> {
      return exposureNotificationClientWrapper.getExposureWindows();
    };

    ListenableFuture<Task<List<ExposureWindow>>> listenableFuture = executor.submit(asyncTask);

    try {
      Task<List<ExposureWindow>> exposureWindowsTask = listenableFuture.get();
      exposureWindowsTask
              .addOnFailureListener(
              e -> Log.e(TAG, "exposureWindows", e))
              .addOnSuccessListener(
              exposureWindows -> {
                List<ExposureWindow> recentExposure = new ArrayList<ExposureWindow>();

                Instant classificationDate = Instant.EPOCH
                        .plus(Duration.ofDays(currentClassification.getClassificationDate()));
                long date = classificationDate.toEpochMilli();

                for (ExposureWindow window: exposureWindows) {
                  if (date ==  window.getDateMillisSinceEpoch()) {
                    recentExposure.add(window);
                  }
                }
                Log.d(TAG, "recentExposure" + recentExposure.toString());
                ExposureClassification updatedClassification = ExposureClassification.create(currentClassification.getClassificationIndex(),
                        currentClassification.getClassificationName(),
                        currentClassification.getClassificationDate(),
                        currentClassification.getDailySummary() + formatExposureWindow(recentExposure));
                exposureNotificationSharedPreferences.setExposureClassification(updatedClassification);
              });

    } catch (ExecutionException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

  private String formatInfectiousness(int value) {
    String infectiousness = "";
    switch(value) {
      case Infectiousness.HIGH: infectiousness = "High"; break;
      case Infectiousness.NONE: infectiousness = "None"; break;
      case Infectiousness.STANDARD: infectiousness = "Standard"; break;
      default: infectiousness = "Unknown";
    }
    return infectiousness;
  }

  private String getCalibrationConfidence(int value) {
    String calibrationConfidence = "Calibration Confidence: ";
    switch (value) {
      case CalibrationConfidence.HIGH:
        calibrationConfidence = "High"; break;
      case CalibrationConfidence.MEDIUM:
        calibrationConfidence = "Medium"; break;
      case CalibrationConfidence.LOW:
        calibrationConfidence = "Low"; break;
      case CalibrationConfidence.LOWEST:
        calibrationConfidence = "Lowest"; break;
      default:
        calibrationConfidence = "Unknown";
    }
    return calibrationConfidence;
  }

  private String getReportType(int type) {
    String reportType = "";
    switch (type) {
      case ReportType.CONFIRMED_CLINICAL_DIAGNOSIS:
        reportType = "Confirmed Clinical Diagnosis"; break;
      case ReportType.CONFIRMED_TEST:
        reportType = "Confirmed Test"; break;
      case ReportType.RECURSIVE:
        reportType = "Recursive"; break;
      case ReportType.REVOKED:
        reportType = "Revoked"; break;
      case ReportType.SELF_REPORT:
        reportType = "Self Reported"; break;
      case ReportType.UNKNOWN:
        reportType = "Unknown"; break;
      default:
        reportType = "Unknown";
    }
    return reportType;
  }

  private String formatExposureWindow(List<ExposureWindow> expWinList) {
    String formattedWindow = "Exposure Windows:\n";
    int counter = 1;
    for (ExposureWindow window: expWinList) {
      Date date=new Date(window.getDateMillisSinceEpoch());
      SimpleDateFormat df2 = new SimpleDateFormat("dd MMM yyyy");
      String dateText = df2.format(date);
      formattedWindow = formattedWindow.concat("--------------------------------------------\n")
                        .concat("Window ").concat(String.valueOf(counter)).concat("\n")
                        .concat("Date: ").concat(dateText).concat("\n")
                        .concat("Calibration Confidence: ").concat(getCalibrationConfidence(window.getCalibrationConfidence())).concat("\n")
                        .concat("Report Type: ").concat(getReportType(window.getReportType())).concat("\n")
                        .concat("Infectiousness: ").concat(formatInfectiousness(window.getInfectiousness())).concat("\n")
                        .concat(formatScanInstances(window.getScanInstances()));
      counter ++;
    }

    return formattedWindow;
  }

  private String formatScanInstances(List<ScanInstance> scanInstanceList) {
    String formatted = "Scan Instances:\n";
    int counter = 1;
    for (ScanInstance instance: scanInstanceList) {
      formatted = formatted.concat("--------------------------------------------\n")
              .concat("Instance #").concat(String.valueOf(counter)).concat("\n")
              .concat("Minimum Attenuation: ").concat(String.valueOf(instance.getMinAttenuationDb())).concat("\n")
              .concat("Seconds Since Last Scan: ").concat(String.valueOf(instance.getSecondsSinceLastScan())).concat("\n")
              .concat("Typical Attenuation: ").concat(String.valueOf(instance.getTypicalAttenuationDb())).concat("\n");
      counter ++;
    }
    return formatted;
  }


  /**
   * Handle dailySummary update logic:
   * <ul>
   * <li>- Update UI's classification and date components
   * <li>- Badge UI components as "new"
   * <li>- Recognize changes that trigger notifications
   * <li>- Detect revocations
   * </ul>
   */
  public void checkForExposureUpdate(Context context,
      List<ExposureEntity> currentExposureEntities, ExposureClassification currentClassification,
      ExposureClassification previousClassification) {

    boolean isClassificationRevoked = false;

    Log.d(TAG, "Current ExposureClassification: " + currentClassification);

    Log.d(TAG, "Previous ExposureClassification: " + previousClassification);

    /*
     * We assume a change of classification if either the classification index (and resources)
     * change (because of changes in the underlying dailySummaries)
     * OR if the classification name changes (when the health authority changes their definitions).
     * This is also the information used to decide on the "new" badges
     */
    boolean newExposureClassification =
        (previousClassification.getClassificationIndex()
            != currentClassification.getClassificationIndex())
            || (!previousClassification.getClassificationName()
            .equals(currentClassification.getClassificationName()));

    boolean newExposureDate =
        (previousClassification.getClassificationDate()
            != currentClassification.getClassificationDate());

    /*
     * If either of these change, we almost always notify the user with the notification message of
     * the current exposure classification. The only exceptions are state
     * changes from "some exposure classification" to "no exposure". In this case we only notify
     * if we believe the state-change was caused by a key revocation.
     * If this transition occurs without a revocation, it is usually caused by exposure windows
     * expiring after 14 days. We don't want to notify the user in this case.
     *
     * To give the UI a chance to update to changed exposure classifications, we only set a
     * "notifyUser" flag here and postpone the actual notification to the very end of this method.
     */
    int notificationTitleResource = 0;
    int notificationMessageResource = 0;

    if (newExposureClassification || newExposureDate) {
      if (previousClassification.getClassificationIndex()
          != ExposureClassification.NO_EXPOSURE_CLASSIFICATION_INDEX
          && currentClassification.getClassificationIndex()
          == ExposureClassification.NO_EXPOSURE_CLASSIFICATION_INDEX) {

        // Check for the revocation edge case by looking up previous exposures in room
        List<ExposureEntity> previousExposureEntities = exposureRepository.getAllExposureEntities();
        if (revocationDetector.isRevocation(previousExposureEntities, currentExposureEntities)) {
          notificationTitleResource = R.string.exposure_notification_title_revoked;
          notificationMessageResource = R.string.exposure_notification_message_revoked;
          isClassificationRevoked = true;
        }
      }
      // Otherwise just notify using the normal classifications
      else {
        notificationTitleResource = getNotificationTitleResource(currentClassification);
        notificationMessageResource = getNotificationMessageResource(currentClassification);
      }

    }

    /*
     * Write the new state to SharedPrefs to detect changes on the next call.
     * Persist information on what was updated / if there was a revocation for the UI
     */
    exposureNotificationSharedPreferences.setExposureClassification(currentClassification);
    exposureNotificationSharedPreferences
        .setIsExposureClassificationRevoked(isClassificationRevoked);
    if (newExposureClassification) {
      exposureNotificationSharedPreferences
          .setIsExposureClassificationNewAsync(BadgeStatus.NEW);
    }
    if (newExposureDate) {
      exposureNotificationSharedPreferences
          .setIsExposureClassificationDateNewAsync(BadgeStatus.NEW);
    }

    /*
     * Notify the user
     */
    if (notificationTitleResource != 0 || notificationMessageResource != 0) {
      notificationHelper.showPossibleExposureNotification(
          context, notificationTitleResource, notificationMessageResource);
      exposureNotificationSharedPreferences
          .setExposureNotificationLastShownClassification(clock.now(),
              currentClassification);
      Log.d(TAG, "Notifying user: "
          + context.getResources().getString(notificationTitleResource) + " - "
          + context.getResources().getString(notificationMessageResource));
    } else {
      Log.d(TAG, "No new exposure information, not notifying user");
    }

    /*
     * Write a the scores of the DailySummaries to disk for later revocation detection
     */
    exposureRepository.clearInsertExposureEntities(currentExposureEntities);
  }

  /**
   * Helper to provide the string resources for the notification title
   */
  private int getNotificationTitleResource(ExposureClassification exposureClassification) {
    switch (exposureClassification.getClassificationIndex()) {
      case 1:
        return R.string.exposure_notification_title_1;
      case 2:
        return R.string.exposure_notification_title_2;
      case 3:
        return R.string.exposure_notification_title_3;
      case 4:
        return R.string.exposure_notification_title_4;
      default:
        throw new IllegalArgumentException("Classification index must be between 1 and 4");
    }
  }

  /**
   * Helper to provide the string resources for the notification message
   */
  private int getNotificationMessageResource(ExposureClassification exposureClassification) {
    switch (exposureClassification.getClassificationIndex()) {
      case 1:
        return R.string.exposure_notification_message_1;
      case 2:
        return R.string.exposure_notification_message_2;
      case 3:
        return R.string.exposure_notification_message_3;
      case 4:
        return R.string.exposure_notification_message_4;
      default:
        throw new IllegalArgumentException("Classification index must be between 1 and 4");
    }
  }
}