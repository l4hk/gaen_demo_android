/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.keydownload;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.concurrent.futures.CallbackToFutureAdapter;

import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.google.android.apps.exposurenotification.common.Qualifiers.BackgroundExecutor;
import com.google.android.apps.exposurenotification.common.Qualifiers.LightweightExecutor;
import com.google.android.apps.exposurenotification.common.Qualifiers.ScheduledExecutor;
import com.google.android.apps.exposurenotification.common.time.Clock;
import com.google.android.apps.exposurenotification.keydownload.Qualifiers.HomeDownloadUriPair;
import com.google.android.apps.exposurenotification.keydownload.Qualifiers.TravellerDownloadUriPairs;
import com.google.android.apps.exposurenotification.logging.AnalyticsLogger;
import com.google.android.apps.exposurenotification.network.RequestQueueWrapper;
import com.google.android.apps.exposurenotification.network.RespondableByteArrayRequest;
import com.google.android.apps.exposurenotification.network.VolleyUtils;
import com.google.android.apps.exposurenotification.proto.RpcCall.RpcCallType;
import com.google.android.apps.exposurenotification.riskcalculation.ClassificationThreshold;
import com.google.android.apps.exposurenotification.riskcalculation.DiagnosisKeyDataMappingHelper;
import com.google.android.apps.exposurenotification.roaming.CountryCodes;
import com.google.android.gms.nearby.exposurenotification.DailySummariesConfig;
import com.google.android.gms.nearby.exposurenotification.DiagnosisKeysDataMapping;
import com.google.android.gms.nearby.exposurenotification.Infectiousness;
import com.google.android.gms.nearby.exposurenotification.ReportType;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.common.collect.ImmutableList;
import com.google.common.io.BaseEncoding;
import com.google.common.util.concurrent.FluentFuture;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import dagger.hilt.android.qualifiers.ApplicationContext;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.Duration;
import com.google.android.apps.exposurenotification.R;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

/**
 * A class to download Diagnosis Key files from one or more servers.
 *
 * <p>Consults one service for users who not travelled outside their home region in the past 14 days
 * (as far as we can tell very roughly from cell network country codes). Consults roaming servers
 * for travellers, if the app is configured to do so by the user's home health authority.
 */
public class DiagnosisKeyDownloader {

  private static final String TAG = "KeyDownloader";
  private static final SecureRandom RAND = new SecureRandom();
  private static final BaseEncoding BASE32 = BaseEncoding.base32().lowerCase().omitPadding();

  private static final String FILE_PATTERN = "/diag_keys/%s/keys_%s.zip";
  private static final Duration DOWNLOAD_ALL_FILES_TIMEOUT = Duration.ofMinutes(30);

  private final Context context;
  private final CountryCodes countryCodes;
  private final KeyFileUriResolver keyFileUriResolver;
  private final DownloadUriPair homeDownloadUris;
  private final Map<String, List<DownloadUriPair>> travellerDownloadUriPairs;
  private final RequestQueueWrapper requestQueueWrapper;
  private final ExecutorService backgroundExecutor;
  private final ExecutorService lightweightExecutor;
  private final ScheduledExecutorService scheduledExecutor;
  private final AnalyticsLogger logger;
  private final Clock clock;

  @Inject
  DiagnosisKeyDownloader(
      @ApplicationContext Context context,
      RequestQueueWrapper requestQueueWrapper,
      CountryCodes countryCodes,
      KeyFileUriResolver keyFileUriResolver,
      @HomeDownloadUriPair DownloadUriPair homeDownloadUris,
      @TravellerDownloadUriPairs Map<String, List<DownloadUriPair>> travellerDownloadUriPairs,
      @BackgroundExecutor ExecutorService backgroundExecutor,
      @LightweightExecutor ExecutorService lightweightExecutor,
      @ScheduledExecutor ScheduledExecutorService scheduledExecutor,
      AnalyticsLogger logger,
      Clock clock) {
    this.context = context;
    this.requestQueueWrapper = requestQueueWrapper;
    this.countryCodes = countryCodes;
    this.keyFileUriResolver = keyFileUriResolver;
    this.homeDownloadUris = homeDownloadUris;
    this.travellerDownloadUriPairs = travellerDownloadUriPairs;
    this.backgroundExecutor = backgroundExecutor;
    this.lightweightExecutor = lightweightExecutor;
    this.scheduledExecutor = scheduledExecutor;
    this.logger = logger;
    this.clock = clock;
  }

  /**
   * Downloads all available files of Diagnosis Keys for the currently applicable regions and
   * returns a future with a list of all the batches of files.
   */
  public ListenableFuture<ImmutableList<KeyFile>> download() {
    ImmutableList.Builder<DownloadUriPair> keyserversToCall =
        ImmutableList.<DownloadUriPair>builder().add(homeDownloadUris);
    // Did the user travel outside their home region in the last 14 days? If so, include their HA's
    // traveller URLs too.
    for (String countryCode : countryCodes.getExposureRelevantCountryCodes()) {
      if (travellerDownloadUriPairs.containsKey(countryCode)) {
        keyserversToCall.addAll(travellerDownloadUriPairs.get(countryCode));
      }
    }

    ListenableFuture<ImmutableList<KeyFile>> downloadedFiles =
        // Start with the user's home region download URIs.
        FluentFuture.from(keyFileUriResolver.resolve(keyserversToCall.build()))
            // Now initiate file downloads for each URI
            .transformAsync(
                this::initiateDownloads,
                backgroundExecutor)
            // It's important to have a timeout since we're waiting for network operations that may
            // or may not complete.
            .withTimeout(
                DOWNLOAD_ALL_FILES_TIMEOUT.toMillis(),
                TimeUnit.MILLISECONDS,
                scheduledExecutor);

    // Add a callback just to log success/failure.
    Futures.addCallback(downloadedFiles, LOG_OUTCOME, backgroundExecutor);

    return downloadedFiles;
  }

  private ListenableFuture<ImmutableList<KeyFile>> initiateDownloads(List<KeyFile> keyFiles) {
    String dir = randDirname();
    List<ListenableFuture<KeyFile>> downloadedFiles = new ArrayList<>();
    int fileCounter = 1;
    for (KeyFile file : keyFiles) {
      String path = String.format(FILE_PATTERN, dir, fileCounter++);
      downloadedFiles.add(downloadAndSave(file, path));
    }
    return FluentFuture.from(Futures.allAsList(downloadedFiles))
        .transform(ImmutableList::copyOf, lightweightExecutor);
  }

  private ListenableFuture<KeyFile> downloadAndSave(KeyFile keyFile, String path) {
    return FluentFuture.from(downloadFile(keyFile.uri()))
        .transformAsync(
            bytes -> {
              try {
                return Futures.immediateFuture(keyFile.with(saveKeyFile(bytes, path)));
              } catch (IOException e) {
                return Futures.immediateFailedFuture(e);
              }
            },
            backgroundExecutor);
  }

  public String downloadConfig() {
      String response = null;

      try {
          URL url = new URL("https://storage.googleapis.com/exposure-notification-export-txasb/config/gaen_config.json");
          HttpURLConnection conn = (HttpURLConnection) url.openConnection();
          Log.d(TAG, "[downloadConfig] conn: " + conn);
          conn.setRequestMethod("GET");
          // read the response
          InputStream in = new BufferedInputStream(conn.getInputStream());
          response = convertStreamToString(in);
          Log.d(TAG, "[downloadConfig] Response from url: " + response);
      } catch (MalformedURLException e) {
          Log.e(TAG, "MalformedURLException: " + e.getMessage());
      } catch (ProtocolException e) {
          Log.e(TAG, "ProtocolException: " + e.getMessage());
      } catch (IOException e) {
          Log.e(TAG, "IOException: " + e.getMessage());
      } catch (Exception e) {
          e.printStackTrace();
          Log.e(TAG, "downloadConfig Exception: " + e.getMessage());
      }
      return response;
  }

  public  ListenableFuture<ClassificationThreshold> downloadThresholdConfig() {
    String response = null;
    ClassificationThreshold thresholdConfig = null;

    try {
        response = downloadConfig();
        thresholdConfig = convertToThresholdConfig(response);
    } catch (Exception e) {
        Log.e(TAG, "Exception: " + e.getMessage());
    }
    return Futures.immediateFuture(thresholdConfig);
  }

  public DiagnosisKeysDataMapping downloadDiagnosisKeysDataMappingConfig() {
    String response = null;
    DiagnosisKeysDataMapping diagnosisKeysDataMapping = null;

    try {
        response = downloadConfig();
        diagnosisKeysDataMapping = convertToDiagnosisKeysDataMapping(response);
    } catch (Exception e) {
        Log.e(TAG, "Exception: " + e.getMessage());
    }
    return diagnosisKeysDataMapping;
  }

  public ListenableFuture<Map<String, Object>> downloadDailySummaryConfig() {
    String response = null;
    DailySummariesConfig dailySummariesConfig = null;
    ClassificationThreshold thresholdConfig = null;
    Map<String, Object> map = new HashMap<String, Object>();
    try {
        ListeningExecutorService executor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(10));

        Callable<String> asyncTask = () -> {
            return downloadConfig();
        };

        ListenableFuture<String> listenableFuture = executor.submit(asyncTask);

        dailySummariesConfig = convertToDailySummaryConfig(listenableFuture.get());
        thresholdConfig = convertToThresholdConfig(listenableFuture.get());
        Log.d(TAG, "dailySummariesConfig: " + dailySummariesConfig.toString());
        Log.d(TAG, "thresholdConfig: " + thresholdConfig.toString());
    } catch (Exception e) {
        Log.e(TAG, "downloadDailySummaryConfig Exception: " + e.getMessage());
    }

    map.put("dailySummariesConfig", dailySummariesConfig);
    map.put("thresholdConfig", thresholdConfig);
    return Futures.immediateFuture(map);
  }

  private String convertStreamToString(InputStream is) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
    StringBuilder sb = new StringBuilder();

    String line;
    try {
        while ((line = reader.readLine()) != null) {
            sb.append(line).append('\n');
        }
    } catch (IOException e) {
        e.printStackTrace();
    } finally {
        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    return sb.toString();
  }
  private ClassificationThreshold convertToThresholdConfig(String jsonStr) {
      Log.d(TAG, "Response from url: " + jsonStr);
      ClassificationThreshold config = null;

      if (jsonStr != null) {
          try {
              JSONObject jsonObj = new JSONObject(jsonStr);
              config = new ClassificationThreshold(1,
              "Threshold",
              jsonObj.getInt("confirmedTestPerDaySumERVThreshold"),
              jsonObj.getInt("clinicalDiagnosisPerDaySumERVThreshold"),
              jsonObj.getInt("selfReportPerDaySumERVThreshold"),
              jsonObj.getInt("recursivePerDaySumERVThreshold"),
              jsonObj.getInt("perDaySumERVThreshold"),
              jsonObj.getInt("perDayMaxERVThreshold"),
              jsonObj.getInt("weightedDurationAtAttenuationThreshold"));

          } catch (final JSONException e) {
              Log.e(TAG, "Json parsing error: " + e.getMessage());
          }
      } else {
          Log.e(TAG, "Couldn't get json from server.");
      }

      return config;
  }

  private DiagnosisKeysDataMapping convertToDiagnosisKeysDataMapping(String jsonStr) {

    Log.d(TAG, "Response from url: " + jsonStr);
    DiagnosisKeysDataMapping diagnosisKeysDataMapping = null;

    if (jsonStr != null) {
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);

            JSONObject infectiousness = jsonObj.getJSONObject("infectiousnessForDaysSinceOnsetOfSymptoms");

            Map<Integer, Integer> daysSinceOnsetToInfectiousness = new HashMap<>();
            for (int day = -14; day <= 14; day++) {
                daysSinceOnsetToInfectiousness.put(day, infectiousness.getInt(String.valueOf(day)));
            }
            int reportTypeWhenMissing = context.getResources().getInteger(R.integer.enx_reportTypeNoneMap);

            diagnosisKeysDataMapping = DiagnosisKeyDataMappingHelper
                    .createDiagnosisKeysDataMapping(daysSinceOnsetToInfectiousness, reportTypeWhenMissing, infectiousness.getInt("unknown"));

        } catch (final JSONException e) {
            Log.e(TAG, "Json parsing error: " + e.getMessage());
        }
    } else {
        Log.e(TAG, "Couldn't get json from server.");
    }

    return diagnosisKeysDataMapping;
  }

  private DailySummariesConfig convertToDailySummaryConfig(String jsonStr) {
      Log.d(TAG, "[convertToDailySummaryConfig] Response from url: " + jsonStr);
      DailySummariesConfig dailySummariesConfig = null;

      if (jsonStr != null) {
          try {
              JSONObject jsonObj = new JSONObject(jsonStr);

              List<Integer> attenuationDurationThresholds = new ArrayList<Integer>();
              try {
                  int i;
                  JSONArray array =  jsonObj.getJSONArray("attenuationDurationThresholds");
                  for (i = 0; i < array.length(); i++)
                      attenuationDurationThresholds.add(array.getInt(i));
              } catch (JSONException e) {
                  System.out.println(e.getMessage());
              }

              double WEIGHT_FACTOR = 0.01;
              dailySummariesConfig = new DailySummariesConfig.DailySummariesConfigBuilder()
                      /*
                       * This puts each scan into four attenuation buckets: Immediate, Near, Medium and Other
                       * Three Bluetooth attenuation thresholds (in dB) are used to define how exposure is divided
                       * between the buckets. These buckets are each weighted with four customizable weights.
                       */
                      .setAttenuationBuckets(
                              /*threshold in db*/
                              attenuationDurationThresholds,
                              Arrays.asList( /*weight*/
                                      jsonObj.getInt("immediateDurationWeight") * WEIGHT_FACTOR,
                                      jsonObj.getInt("nearDurationWeight") * WEIGHT_FACTOR,
                                      jsonObj.getInt("mediumDurationWeight") * WEIGHT_FACTOR,
                                      jsonObj.getInt("otherDurationWeight") * WEIGHT_FACTOR))
                      /*
                       * Each window gets assigned a Infectiousness (NONE, STANDARD, HIGH) depending on the days
                       * since symptom onset. STANDARD and HIGH levels can be assigned a weight between 0 and 250%
                       * (0.0 - 2.5), NONE is fixed at 0.0
                       */
                      .setInfectiousnessWeight(Infectiousness.STANDARD,
                              jsonObj.getInt("infectiousnessStandardWeight") * WEIGHT_FACTOR)
                      .setInfectiousnessWeight(Infectiousness.HIGH,
                              jsonObj.getInt("infectiousnessHighWeight") * WEIGHT_FACTOR)
                      /*
                       * Each type of report can be assigned different weights. This is helpful
                       * to e.g. prioritize confirmed clinical diagnoses over self-reported infections.
                       * There are two additional ReportTypes that we don't configure here:
                       *  - ReportType.REVOKED is used to revoke keys (and thus does not need a weight)
                       *  - ReportType.RECURSIVE which is not supported by the configuration tool (yet)
                       */
                      .setReportTypeWeight(ReportType.CONFIRMED_TEST,
                              jsonObj.getInt("reportTypeConfirmedTestWeight") * WEIGHT_FACTOR)
                      .setReportTypeWeight(ReportType.CONFIRMED_CLINICAL_DIAGNOSIS,
                              jsonObj.getInt("reportTypeConfirmedClinicalDiagnosisWeight")
                                      * WEIGHT_FACTOR)
                      .setReportTypeWeight(ReportType.SELF_REPORT,
                              jsonObj.getInt("reportTypeSelfReportedWeight") * WEIGHT_FACTOR)
                      /*
                       * Filtering: For how many days since exposure should exposure windows be included?
                       * E.g. the value 10 only includes exposures from the last 10 days.
                       */
                      .setDaysSinceExposureThreshold(context.getResources().getInteger(R.integer.enx_daysSinceExposureThreshold))
                      /*
                       * Filtering: Remove windows with a score lower than x. This is not supplied by the
                       * HA/configuration, so we don't set our own value and rather use defaults here.
                       * .setMinimumWindowScore(0)
                       */
                      .build();
          } catch (final JSONException e) {
              Log.e(TAG, "Json parsing error: " + e.getMessage());
          }
      } else {
          Log.e(TAG, "Couldn't get json from server.");
      }

      return dailySummariesConfig;
  }

  private ListenableFuture<byte[]> downloadFile(Uri uri) {
    return CallbackToFutureAdapter.getFuture(
        completer -> {
          Listener<byte[]> responseListener =
              response -> {
                Log.d(
                    TAG,
                    "Keyfile " + uri + " successfully downloaded " + response.length + " bytes.");
                completer.set(response);
              };

          ErrorListener errorListener =
              err -> {
                Log.e(TAG, "Error getting keyfile " + uri);
                completer.setException(err);
              };

          Log.d(TAG, "Downloading keyfile file from " + uri);
          RespondableByteArrayRequest request =
              new RespondableByteArrayRequest(uri, responseListener, errorListener, clock);
          requestQueueWrapper.add(request);
          return request;
        });
  }

  private File saveKeyFile(byte[] content, String path) throws IOException {
    File toFile = new File(context.getFilesDir(), path);
    FileUtils.writeByteArrayToFile(toFile, content);
    return toFile;
  }

  private static String randDirname() {
    byte[] bytes = new byte[8];
    RAND.nextBytes(bytes);
    return BASE32.encode(bytes);
  }

  private final FutureCallback<ImmutableList<KeyFile>> LOG_OUTCOME =
      new FutureCallback<ImmutableList<KeyFile>>() {
        @Override
        public void onSuccess(@NullableDecl ImmutableList<KeyFile> files) {
          int totalBytesDownloaded = 0;
          for (KeyFile file : files) {
            totalBytesDownloaded += file.file().length();
          }
          logger.logRpcCallSuccess(RpcCallType.RPC_TYPE_KEYS_DOWNLOAD, totalBytesDownloaded);
        }

        @Override
        public void onFailure(@NonNull Throwable t) {
          logger.logRpcCallFailure(RpcCallType.RPC_TYPE_KEYS_DOWNLOAD, t);
          Log.d(TAG, VolleyUtils.getErrorBodyWithoutPadding(t).toString());
        }
      };
}

